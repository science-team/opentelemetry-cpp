Source: opentelemetry-cpp
Section: libs
Priority: optional
Maintainer: Freexian Packaging Team <team+freexian@tracker.debian.org>
Uploaders:
 Santiago Ruano Rincón <santiago@freexian.com>,
Rules-Requires-Root: no
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 libabsl-dev,
 libbenchmark-dev,
 libcurl4-openssl-dev,
 libgmock-dev,
 libgrpc++-dev,
 libgtest-dev,
 libprotobuf-dev,
 nlohmann-json3-dev,
 opentelemetry-proto,
 protobuf-compiler-grpc,
 zlib1g-dev,
Standards-Version: 4.7.0.1
Homepage: https://github.com/open-telemetry/opentelemetry-cpp/
Vcs-Browser: https://salsa.debian.org/science-team/opentelemetry-cpp
Vcs-Git: https://salsa.debian.org/science-team/opentelemetry-cpp.git

Package: opentelemetry-cpp-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 opentelemetry-cpp (= ${binary:Version}),
 ${misc:Depends},
Description: OpenTelemetry C++ client library
 OpenTelemetry is an Observability framework and toolkit designed to create and
 manage telemetry data such as traces, metrics, and logs. Crucially,
 OpenTelemetry is vendor- and tool-agnostic, meaning that it can be used with a
 broad variety of Observability backends.
 .
 OpenTelemetry is focused on the generation, collection, management, and export
 of telemetry. A major goal of OpenTelemetry is that you can easily instrument
 your applications or systems, no matter their language, infrastructure, or
 runtime environment. The storage and visualization of telemetry is
 intentionally left to other tools.
 .
 This package provides the C++ OpenTelemetry client header-only library.

Package: opentelemetry-cpp
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: OpenTelemetry C++ SDK library
 OpenTelemetry is an Observability framework and toolkit designed to create and
 manage telemetry data such as traces, metrics, and logs. Crucially,
 OpenTelemetry is vendor- and tool-agnostic, meaning that it can be used with a
 broad variety of Observability backends.
 .
 OpenTelemetry is focused on the generation, collection, management, and export
 of telemetry. A major goal of OpenTelemetry is that you can easily instrument
 your applications or systems, no matter their language, infrastructure, or
 runtime environment. The storage and visualization of telemetry is
 intentionally left to other tools.
 .
 This package provides the C++ OpenTelemetry SDK library.
